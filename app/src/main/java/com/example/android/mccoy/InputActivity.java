package com.example.android.mccoy;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.content.Intent;
import android.view.View;
import android.widget.EditText;

public class InputActivity extends AppCompatActivity {

    EditText mEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);
    }

    public void clickStartBtn(View view) {
        mEdit   = (EditText)findViewById(R.id.participant_id);
        String participantid = mEdit.getText().toString();
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("participantId", participantid);
        startActivity(intent);
    }
}
