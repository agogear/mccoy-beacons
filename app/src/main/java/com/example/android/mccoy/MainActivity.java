package com.example.android.mccoy;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.estimote.coresdk.common.requirements.SystemRequirementsChecker;
import com.estimote.coresdk.observation.region.RegionUtils;
import com.estimote.coresdk.observation.region.beacon.BeaconRegion;
import com.estimote.coresdk.recognition.packets.Beacon;
import com.estimote.coresdk.recognition.utils.MacAddress;
import com.estimote.coresdk.service.BeaconManager;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private BeaconManager beaconManager;
    private BeaconRegion region;

    private DatabaseReference mDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Bundle bundle = getIntent().getExtras();
        final String participantId = bundle.getString("participantId");

        final TextView numTextView = (TextView) findViewById(R.id.num);
        numTextView.setText(participantId);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        beaconManager = new BeaconManager(this);
        beaconManager.setRangingListener(new BeaconManager.BeaconRangingListener() {

            @Override
            public void onBeaconsDiscovered(BeaconRegion beaconRegion, List<Beacon> beacons) {
                int num = beacons.size();
                if (num != 0) {
                    Beacon beacon = beacons.get(0);
                    UUID uuid = beacon.getProximityUUID();
                    String uuidStr = uuid.toString();

                    MacAddress macAddress = beacon.getMacAddress();
                    String macAddressStr = macAddress.toString();
                    double distance = RegionUtils.computeAccuracy(beacon);
                    List<Double> coordinates = getBeaconCoordinates(beacon);

                    long timestamp = System.currentTimeMillis();

                    Position position = new Position(uuidStr, macAddressStr, timestamp, distance, coordinates);

                    mDatabase.child("positions").child(participantId).push().setValue(position);

                    Log.d("Beacon", "UUID: " + uuid + "\n" + "Distance: " + distance);
                    numTextView.setText("UUID: " + uuid + "\n" + "Distance: " + distance);

                } else {
                    numTextView.setText("None beacon");
                }
            }
        });

        region = new BeaconRegion("ranged region", null, null, null);
    }

    @Override
    protected void onResume() {
        super.onResume();
        SystemRequirementsChecker.checkWithDefaultDialogs(this);

        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                beaconManager.startRanging(region);
            }
        });
    }

    @Override
    protected void onPause() {
        beaconManager.stopRanging(region);

        super.onPause();
    }

    private static final Map<String, List<Double>> BEACONS_COORDINATES;

    private static final String BEACON1 = "53709:61697";
    private static final String BEACON2 = "6459:52861";
    private static final String BEACON3 = "47010:38434";
    private static final String BEACON4 = "58537:5550";
    private static final String BEACON5 = "59270:33018";
    private static final String BEACON6 = "46300:44649";
    private static final String BEACON7 = "55620:33252";
    private static final String BEACON8 = "18831:40950";
    private static final String BEACON9 = "7134:18510";
    private static final String BEACON10 = "44590:31127";
    private static final String BEACON11 = "9008:52820";
    private static final String BEACON12 = "55538:43352";
    private static final String BEACON13 = "14606:62763";
    private static final String BEACON14 = "10681:28932";
    private static final String BEACON15 = "54646:64911";
    private static final String BEACON16 = "10720:12037";
    private static final String BEACON17 = "61228:51111";
    private static final String BEACON18 = "13654:31849";
    private static final String BEACON19 = "39945:26673";
    private static final String BEACON20 = "1374:15748";

    static {
        Map<String, List<Double>> coordinates = new HashMap<>();
        //BEACON test1
        coordinates.put(BEACON1, new ArrayList<Double>() {{
            add(8.0);
            add(11.0);
        }
        });

        //BEACON test2
        coordinates.put(BEACON2, new ArrayList<Double>() {{
            add(7.0);
            add(18.0);
        }
        });

        //BEACON test3
        coordinates.put(BEACON3, new ArrayList<Double>() {{
            add(9.0);
            add(23.0);
        }
        });

        //BEACON test4
        coordinates.put(BEACON4, new ArrayList<Double>() {{
            add(13.0);
            add(21.0);
        }
        });

        //BEACON test5
        coordinates.put(BEACON5, new ArrayList<Double>() {{
            add(10.0);
            add(24.0);
        }
        });

        //BEACON test6
        coordinates.put(BEACON6, new ArrayList<Double>() {{
            add(12.0);
            add(32.0);
        }
        });

        //BEACON test7
        coordinates.put(BEACON7, new ArrayList<Double>() {{
            add(19.0);
            add(25.0);
        }
        });

        //BEACON test8
        coordinates.put(BEACON8, new ArrayList<Double>() {{
            add(20.0);
            add(31.0);
        }
        });

        //BEACON test9
        coordinates.put(BEACON9, new ArrayList<Double>() {{
            add(32.0);
            add(31.0);
        }
        });

        //BEACON test10
        coordinates.put(BEACON10, new ArrayList<Double>() {{
            add(31.0);
            add(21.0);
        }
        });

        //BEACON test11
        coordinates.put(BEACON11, new ArrayList<Double>() {{
            add(23.0);
            add(13.0);
        }
        });

        //BEACON test12
        coordinates.put(BEACON12, new ArrayList<Double>() {{
            add(31.0);
            add(12.0);
        }
        });

        //BEACON test13
        coordinates.put(BEACON13, new ArrayList<Double>() {{
            add(24.0);
            add(7.0);
        }
        });

        //BEACON test14
        coordinates.put(BEACON14, new ArrayList<Double>() {{
            add(32.0);
            add(6.0);
        }
        });

        //BEACON test15
        coordinates.put(BEACON15, new ArrayList<Double>() {{
            add(32.0);
            add(3.0);
        }
        });

        //BEACON test16
        coordinates.put(BEACON16, new ArrayList<Double>() {{
            add(17.0);
            add(4.0);
        }
        });

        //BEACON test17
        coordinates.put(BEACON17, new ArrayList<Double>() {{
            add(19.0);
            add(16.0);
        }
        });

        //BEACON test18
        coordinates.put(BEACON18, new ArrayList<Double>() {{
            add(18.0);
            add(22.0);
        }
        });

        //BEACON test19
        coordinates.put(BEACON19, new ArrayList<Double>() {{
            add(14.0);
            add(12.0);
        }
        });

        //BEACON test20
        coordinates.put(BEACON20, new ArrayList<Double>() {{
            add(17.0);
            add(7.0);
        }
        });

        BEACONS_COORDINATES = Collections.unmodifiableMap(coordinates);
    }

    private List<Double> getBeaconCoordinates(Beacon beacon) {
        String beaconKey = String.format("%d:%d", beacon.getMajor(), beacon.getMinor());
        if (BEACONS_COORDINATES.containsKey(beaconKey)) {
            return BEACONS_COORDINATES.get(beaconKey);
        }
        return Collections.emptyList();
    }
}
