package com.example.android.mccoy;

import android.app.Application;

import com.estimote.coresdk.common.config.EstimoteSDK;
/**
 * Created by tingxuanz on 18/10/17.
 */

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        EstimoteSDK.initialize(getApplicationContext(), "mccoy-mkk", "e6ceef8f350d967f093f4c49c1af0293");
    };
}
