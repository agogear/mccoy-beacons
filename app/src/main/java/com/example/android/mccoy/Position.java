package com.example.android.mccoy;

import java.util.List;

/**
 * Created by tingxuanz on 31/10/17.
 */

public class Position {
    public String beaconUUID;
    public String beaconMacAddress;
    public long timestamp;
    public double distanceToBeacon;
    public List<Double> beaconCoordinates;

    public Position() {

    }

    public Position(String uuid, String macAddress, long timestamp, double distance, List<Double> coordinates) {
        this.beaconUUID = uuid;
        this.beaconMacAddress = macAddress;
        this.timestamp = timestamp;
        this.distanceToBeacon = distance;
        this.beaconCoordinates = coordinates;
    }
}
